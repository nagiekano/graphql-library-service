package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/nagiekano/library-service-graphql/internal/delivery/resolver/generated"
	"gitlab.com/nagiekano/library-service-graphql/internal/delivery/resolver/model"
)

func (r *mutationResolver) CreateBook(ctx context.Context, input *model.BookInput) (*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) UpdateBook(ctx context.Context, id int, input model.BookInput) (*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) DeleteBook(ctx context.Context, id int) (*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) CreateReader(ctx context.Context, input model.ReaderInput) (*model.Reader, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) UpdateReader(ctx context.Context, id int, input model.ReaderInput) (*model.Reader, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) DeleteReader(ctx context.Context, id int) (*model.Reader, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) BookByID(ctx context.Context, id int) (*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) SearchBookByTitle(ctx context.Context, title string) ([]*model.Book, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) ReaderByID(ctx context.Context, id int) (*model.Reader, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) SearchReaderByName(ctx context.Context, name string) ([]*model.Reader, error) {
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
